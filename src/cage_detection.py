#!/usr/bin/env python3
import sys
import cv2 as cv
import numpy as np
import math
# Potrebna su dva argumenta za rad programa, slika ili video nad kojim se vrsi detekcija
# te uvjet 0 ili 1 ovisno o tome koristimo li funkciju provjere pozicije kaveza


def main(argv):
    if len(argv) < 1:
        return -1
    cap = cv.VideoCapture(0)  
    fourcc = cv.VideoWriter_fourcc(*'XVID')
    out = cv.VideoWriter('output6.avi', fourcc, 10.0, (856, 480))
    cap = cv.VideoCapture(argv[0])
    check_position = int(argv[1])
    count = 0
    key = cv.waitKey(1)

    while key != 27:

        hasFrame, src = cap.read()
        if not hasFrame:
            cv.waitKey()
            break

        count += 1
        frame = src
        cv.putText(frame, "status: Returning home", (20, 50), cv.FONT_HERSHEY_SIMPLEX, 0.6,
                   (152,251,152), 2, cv.LINE_AA)
        out.write(frame) 
        cv.imshow("frame", frame)
        if cv.waitKey(100) & 0xFF == ord('q'):
            break
    
    # Close the window / Release webcam
    cap.release()
  
# After we release our webcam, we also release the output
    out.release() 

    return 0


def intersection(a1, a2, b1, b2):
    s = np.vstack([a1, a2, b1, b2])
    h = np.hstack((s, np.ones((4, 1))))
    l1 = np.cross(h[0], h[1])
    l2 = np.cross(h[2], h[3])
    x, y, z = np.cross(l1, l2)
    if z == 0:
        # lines are parallel
        return (float('inf'), float('inf'))
    return (x/z, y/z)


def nadjiNarancasto(img):  # nadje narancaste stvari na slici
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    # mask = cv.inRange(hsv,(10, 100, 20), (25, 255, 255) )   #RADI ZA VECI RANGE NARANCASTE
    mask = cv.inRange(hsv, (10, 100, 200), (25, 255, 255))

    kernel = np.ones((3, 3), np.uint8)
    mask = cv.erode(mask, kernel, iterations=1)
    mask = cv.dilate(mask, kernel, iterations=5)
    contours, _ = cv.findContours(
        mask.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    rijecnik_bove = {}
    id_b = 0
    for contour in contours:
        approx = cv.approxPolyDP(
            contour, 0.01 * cv.arcLength(contour, True), True)
        if len(approx) >= 8:
            cv.drawContours(img, [approx], 0, (10, 0, 200), 5)
            x = approx.ravel()[0]
            y = approx.ravel()[1] - 5
            cv.putText(img, "bova", (x, y),
                       cv.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0))
            x, y, w, h = cv.boundingRect(approx)

            rijecnik_bove[id_b] = [x, y, w, h]
            id_b = id_b + 1
    return rijecnik_bove


def nadiKavez(img):
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    lower_white = np.array([0, 0, 200])
    # lower_white = np.array([0,0,180])  #OVO radi za veci range
    higher_white = np.array([255, 50, 255])
    mask = cv.inRange(hsv, lower_white, higher_white)
    kernel = np.ones((3, 3), np.uint8)
    mask = cv.erode(mask, kernel, iterations=1)
    mask = cv.dilate(mask, kernel, iterations=10)
    contours, _ = cv.findContours(
        mask.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    dic_kavez = {}
    id_k = 0
    for contour in contours:
        # arc l contour length and closed shake
        approx = cv.approxPolyDP(
            contour, 0.01 * cv.arcLength(contour, True), True)
        x = approx.ravel()[0]
        y = approx.ravel()[1]
        if len(approx) >= 5:
            cv.drawContours(img, [approx], 0, (100, 0, 200), 5)
            cv.putText(img, "kavez", (x, y),
                       cv.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0))
            tocka = (x, y)  # KOJA TREBA

            #cv.circle(img, (x, y), 10, (0, 255 ,255), 3)
            x, y, w, h = cv.boundingRect(approx)
            # kad se vidi cijeli kavez je dobto, treba jos namjesitit
            srediste = (int((x + x + w) / 2), int((y + y + h) / 2))
            cv.circle(img, srediste, 10, (0, 100, 255), 3)
            cv.rectangle(img, (x, y), (x + w, y + h), (255, 255, 255), 4)
            dic_kavez[id_k] = [srediste[0], srediste[1], w, h, x, y]
    return dic_kavez


def provjeri(dic_kavez, dic_bove, frame):
    tocka = [False, None, None]
    # print(dic_kavez)
    # print(dic_bove)
    if len(dic_bove) >= 4:
        if len(dic_kavez) > 0:
            # print("DA")
            tocka = [True,428- int(dic_kavez[0][0]), 240 -int(dic_kavez[0][1]) ]
            cv.circle(frame, (428, 240), 10, (0, 0, 255), -1)
            cv.line(frame, (int(dic_kavez[0][0]), int(dic_kavez[0][1])),
                    (428, 240), (255, 255, 0), 2)
    """        lijeva = dic_bova[0]
            for bova in dic_bove:
                if lijeva[1] > dic_bova[bova][1] :
                    lijeva = bova
            #tocka_bova = (lijeva[0], lijeva[1])
            tocka_bova = (dic_kavez[0][0], 0)
            srediste = (dic_kavez[0][0], dic_kavez[0][1])
            tocka_pomoc = (dic_kavez[0][4], dic_kavez[0][5])
            tocka_pomoc_dva= (0, dic_kavez[0][5])
            tocka = intersection(tocka_bova, srediste, tocka_pomoc, tocka_pomoc_dva)
            tocka = (int(tocka[0]), int(tocka[1]))"""
    return tocka


def find_pomak(dic_bova, dic_kavez, nasli, frame):
    if nasli == True:  # ima 4 bove i postoji kavez, 1 je kavez pa onda ne provjeravamojesu li bove od tog kaveza
        prva_bova = dic_bova[0]
        druga_bova = dic_bova[1]
        treca_bova = dic_bova[2]
        cetvrta_bova = dic_bova[3]
        sjecista = []
        nasli_sjeciste = False
        tocka_A = (prva_bova[0], prva_bova[1])
        tocka_B = (druga_bova[0], druga_bova[1])
        tocka_C = (treca_bova[0], treca_bova[1])
        tocka_D = (cetvrta_bova[0], cetvrta_bova[1])

        # AB i CD
        s1 = intersection(tocka_A, tocka_B, tocka_C, tocka_D)
        sjecista.append(s1)
        # AC i BD
        s2 = intersection(tocka_A, tocka_C, tocka_B, tocka_D)
        sjecista.append(s2)
        # AD i BC
        s3 = intersection(tocka_A, tocka_D, tocka_B, tocka_C)
        sjecista.append(s3)

        w = 800
        h = 1400
        print(sjecista)
        for s in sjecista:
            if s[0] < 0 or s[1] < 0:
                continue
            else:
                sjeciste = s
                nasli_sjeciste = True

        print(str(sjecista))
        print("sjeciste: " + str(sjeciste))
        cv.circle(frame, (int(sjeciste[0]), int(
            sjeciste[1])), 10, (0, 255, 255), 3)
        tolerancija = 7
        if nasli_sjeciste:
            pomak = math.sqrt(pow(sjeciste[0] - dic_kavez[0][0], 2)) + \
                math.sqrt(pow(sjeciste[1] - dic_kavez[0][1], 2))
            if pomak < tolerancija:  # toleriramo pogresku
                print("nema pomaka")
            else:
                #cv.line(frame, (int(sjeciste[0]), int(sjeciste[1])), (dic_kavez[kavez][0], dic_kavez[kavez][1]), (0, 255, 0), 7)
                print("pomak je " + str(pomak))

        count_one = 0
        for bova in dic_bova:
            if count_one == 3:
                break
            count_one = count_one + 1
            # spojiti svaku sa svakom
            cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]),
                    (prva_bova[0], prva_bova[1]), (255, 0, 255), 3)
            cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]),
                    (druga_bova[0], druga_bova[1]), (255, 0, 255), 3)
            cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]),
                    (treca_bova[0], treca_bova[1]), (255, 0, 255), 3)
            cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]),
                    (cetvrta_bova[0], cetvrta_bova[1]), (255, 0, 255), 3)

if __name__ == "__main__":
    main(sys.argv[1:])