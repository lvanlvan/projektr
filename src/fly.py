#!/usr/bin/env python
from __future__ import print_function
import math
from pickle import FALSE, TRUE
from re import A
import matplotlib.pyplot as plt

import roslib
import sys
import rospy
import numpy as np
import time
import cv2 as cv
from geometry_msgs.msg import Twist
from rospy.timer import sleep
from std_msgs.msg import Empty
from std_msgs.msg import Int16  # For error/angle plot publishing
from sensor_msgs.msg import Image
# For battery percentage
from bebop_msgs.msg import CommonCommonStateBatteryStateChanged
from cv_bridge import CvBridge, CvBridgeError
from nav_msgs.msg import Odometry
from math import *
import cage_detection
import lawnMower
from tf.transformations import euler_from_quaternion


class Fly():
    def __init__(self):
        rospy.on_shutdown(self.exitAndLand)
        # inicijalizacija svih potrebnih parametara Bebopa
        self.rate = rospy.Rate(10)
        self.twist = Twist()
        self.line_distance_x = 0
        self.line_angle_rad = 0
        self.twist.linear.x = 0
        self.twist.linear.y = 0
        self.twist.linear.z = 0
        self.twist.angular.x = 0
        self.twist.angular.y = 0
        self.twist.angular.z = 0
        self.frame = 0
        self.x = 0
        self.y = 0
        self.z = 0
        self.roll = 0
        self.pitch = 0
        self.yaw = 0
        self.previous_x = 0
        self.orientation_z = 0
        self.kP = 0.5
        # status = 1 za trazenje kaveza
        # status = 2 za pozicioniranje drona nad kavezom
        # status = 3 za slijedenje kaveza
        # status = 4 za odmicanje od kaveza
        # status = 5 za okretanje
        self.status = 1
        self.bridge = CvBridge()
        self.frame_count = 0
        self.battery = 0
        self.position = 0  # treba li nam ovo?
        self.pub_vel = rospy.Publisher('bebop/cmd_vel', Twist, queue_size=1)
        self.pub_takeoff = rospy.Publisher(
            'bebop/takeoff', Empty, queue_size=1, latch=True)
        self.pub_land = rospy.Publisher('bebop/land', Empty, queue_size=1)
        self.sub_odom = rospy.Subscriber(
            'bebop/odom', Odometry, self.odom_callback)
        self.sub_camera = rospy.Subscriber(
            "bebop/image_raw", Image, self.camera_callback)
        self.pub_camera_control = rospy.Publisher(
            '/bebop/camera_control', Twist, queue_size=1)
        self.sub_battery = rospy.Subscriber('/bebop/states/common/CommonState/BatteryStateChanged',
                                            CommonCommonStateBatteryStateChanged, self.battery_callback)

    def fly(self):
        self.route_poits = lawnMower.lawnMower(
            self.x, self.y, self.yaw, 3, 2)
        self.stop(3)
        self.get_up(1.7)

        for i in range(1, len(self.route_poits[0])):
            if i % 2 == 1:
                self.status = 1
            else :
                self.status = 5
            self.go_to_point(self.route_poits[0][i], self.route_poits[1][i])
        print("Taking off...")
        self.get_up(1)
        # self.stop (10)
        # self.follow_cage_line()
        # #self.stop(3)
        # self.get_up(1)
        self.land()

    def battery_callback(self, data):
        self.battery = data.percent

    def odom_callback(self, msg):
        self.x = msg.pose.pose.position.x
        self.y = msg.pose.pose.position.y
        self.z = msg.pose.pose.position.z
        orientation_q = msg.pose.pose.orientation
        orientation_list = [orientation_q.x,
                            orientation_q.y, orientation_q.z, orientation_q.w]
        (self.roll, self.pitch, self.yaw) = euler_from_quaternion(orientation_list)

    def camera_callback(self, msg):
        self.cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        # if(self.line_follow):
        if self.status == 1:
            self.image_process_cage()
            cv.putText(self.cv_image, "status: Searching for cage", (20, 50), cv.FONT_HERSHEY_SIMPLEX, 0.8,
                   (152,251,152), 2, cv.LINE_AA)
        elif self.status == 2:
            self.image_process_cage()
            cv.putText(self.cv_image, "status: Moving towards the cage center", (20, 50), cv.FONT_HERSHEY_SIMPLEX, 0.8,
                   (152,251,152), 2, cv.LINE_AA)
        elif self.status == 3:
            self.image_process_line()
            cv.putText(self.cv_image, "status: Following cage", (20, 50), cv.FONT_HERSHEY_SIMPLEX, 0.6,
                   (152,251,152), 2, cv.LINE_AA)
        elif self.status == 4:
            cv.putText(self.cv_image, "status: Moving away from the cage center", (20, 50), cv.FONT_HERSHEY_SIMPLEX, 0.6,
                   (152,251,152), 2, cv.LINE_AA)
        # elif(self.cage_detection):
        cv.putText(self.cv_image, "battery: " + str(self.battery) + "%", (20, 20), cv.FONT_HERSHEY_SIMPLEX, 0.8,
                   (152,251,152), 2, cv.LINE_AA)
        cv.imshow("/bebop/image_raw", self.cv_image)
        cv.waitKey(1)
        # print(self.cv_image.shape)

    def image_process_cage(self):
        self.frame_count += 1

        frame = self.cv_image

        nasli = False  # nasli = True oznacava da je pronaden kavez i 4 bove
        if self.frame_count % 1 == 0 or self.frame_count == 1:
            rjecnik_bove = cage_detection.nadjiNarancasto(frame)  # trazi bove
            rjecnik_kavez = cage_detection.nadiKavez(frame)  # trazi kavez

            tocka = cage_detection.provjeri(rjecnik_kavez, rjecnik_bove, frame)
            if tocka[0]:
                self.cage_point = (tocka[1], tocka[2])
                self.status = 2
            #print("tocka:", self.cage_point)
        self.cv_image = cv.resize(frame, (856, 480))

    def image_process_line(self):
        #self.cv_image = self.cv_image[12:468, 21:834]
        self.cv_image = cv.resize(self.cv_image, (856, 480))
        self.cv_image_rgb = cv.cvtColor(self.cv_image, cv.COLOR_BGR2RGB)
        mask = cv.inRange(self.cv_image_rgb, (130, 130, 130), (255, 255, 255))
        kernel = np.ones((3, 3), np.uint8)
        mask = cv.erode(mask, kernel, iterations=1)
        mask = cv.dilate(mask, kernel, iterations=5)
        contours, _ = cv.findContours(mask.copy(),
                                      cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        if(len(contours)) > 0:
            maxContour = contours[0]
            for contour in contours:
                if(cv.minAreaRect(maxContour)[1][0] < cv.minAreaRect(contour)[1][0]):
                    maxContour = contour
            cv.drawContours(self.cv_image, contours, -1, (0, 255, 0), 3)
            line = cv.minAreaRect(maxContour)
            (x_min, y_min), (w_min, h_min), angle = line
            if (w_min < h_min):
                angle = angle - 90
            self.line_angle_rad = ((angle + 90) * -1) * math.pi/180
            self.line_distance_x = x_min - 540
            box = cv.boxPoints(line)
            box = np.int0(box)
            cv.circle(self.cv_image, (int(x_min), int(y_min)),
                      10, (255, 0, 255), -1)
            cv.circle(self.cv_image, (540, 240), 10, (0, 0, 255), -1)
            cv.line(self.cv_image, (int(x_min), int(y_min)),
                    (540, 240), (255, 255, 0), 2)
            cv.drawContours(self.cv_image, [box], 0, (0, 0, 255), 3)
            #print(x_min, y_min, w_min, h_min, angle )

    def go_to_cage(self, next_point_x, next_point_y):
        self.stop()
        print("Cage found, going to center...")
        distance_x = self.cage_point[0]
        distance_y = self.cage_point[1]
        
        distance = sqrt(pow(distance_x, 2) + pow(distance_y, 2))
        print("distance to center:", distance)
        start_yaw = self.yaw

        while distance > 20 and not rospy.is_shutdown():
            distance_x = self.cage_point[0] - self.x
            distance_y = self.cage_point[1] - self.y
            distance = sqrt(pow(distance_x, 2) + pow(distance_y, 2))
            self.twist.linear.y = 0.01 * distance_x / abs(distance_x)
            self.twist.linear.x = 0.01 * distance_y / abs(distance_y)
            self.twist.angular.z = self.normalize_angle(start_yaw-self.yaw)
            self.pub_vel.publish(self.twist)
            self.rate.sleep()

        self.stop()
        self.get_up(0.8)
        self.status = 3
        self.turn(pi/2)
        self.follow_cage_line(next_point_x, next_point_y)

    def follow_cage_line(self, next_point_x, next_point_y):
        
        print("Follow cage started...")
        print("position z:", self.z)
        print("Descending...")
        flag = 0  # dron se nije udaljio jos ako je 0
        for i in range(30):
            self.twist.linear.z = -0.5
            self.pub_vel.publish(self.twist)
            print("position z:", self.z)
            self.rate.sleep()
        self.stop(0.2)
        print("position z:", self.z)

        start_x = self.x
        start_y = self.y
        distance = sqrt(pow((self.x - start_x), 2) +
                        pow((self.y - start_y), 2))
        self.twist.linear.x = 0.03
        self.twist.linear.z = -0.02
        while not rospy.is_shutdown():
            if distance > 0.75 and flag == 0:
                flag = 1
                print("prvi if")
            elif distance < 0.4 and flag == 1:
                print("drugi if")
                break
            distance = sqrt(pow((self.x - start_x), 2) +
                            pow((self.y - start_y), 2))
            if(self.line_distance_x < -250):
                self.twist.linear.y = 0.063 * (self.line_distance_x / -540)
            elif(self.line_distance_x < -10):
                self.twist.linear.y = 0.06 * (self.line_distance_x / -540)
            elif(self.line_distance_x > 10):
                self.twist.linear.y = 0.063 * (self.line_distance_x / -316)
            else:
                self.twist.linear.y = 0
            self.twist.angular.z = 0.85 * self.line_angle_rad
            self.pub_vel.publish(self.twist)
            self.rate.sleep()
        self.stop()
        self.status = 4
        self.get_up(1.8)
        self.go_to_point(next_point_x, next_point_y)
        self.status = 1
        

    def stop(self, duration=0):
        self.twist.linear.x = 0
        self.twist.linear.y = 0
        self.twist.linear.z = 0
        self.twist.angular.x = 0
        self.twist.angular.y = 0
        self.twist.angular.z = 0
        self.pub_vel.publish(self.twist)
        time.sleep(duration)

    def get_up(self, attitude):
        while abs(attitude-self.z) > 0.1 and not rospy.is_shutdown():
            print(self.z)
            self.twist.linear.z = (attitude-self.z)*0.5
            self.pub_vel.publish(self.twist)
            self.rate.sleep()
        self.stop(1)

    def go_to_point(self, goal_x, goal_y):
        self.stop(0.3)
        inc_x = goal_x - self.x
        inc_y = goal_y - self.y
        angle_to_goal = atan2(inc_y, inc_x)

        self.turn(self.normalize_angle(angle_to_goal - self.yaw))

        print("going to point (x =", goal_x, "y =", goal_y, ")")

        distance = sqrt(pow((self.x - goal_x), 2) +
                        pow((self.y - goal_y), 2))
        while not rospy.is_shutdown() and distance > 0.15 and self.status != 2:
            inc_x = goal_x - self.x
            inc_y = goal_y - self.y
            angle_to_goal = self.normalize_angle(atan2(inc_y, inc_x))
            distance = sqrt(pow((self.x - goal_x), 2) +
                            pow((self.y - goal_y), 2))

            if abs(angle_to_goal - self.yaw) > 0.07:
                print("Theta =", angle_to_goal, "Razlika kuta:",
                      self.normalize_angle(angle_to_goal - self.yaw))
                self.twist.linear.x = 0
                self.twist.linear.y = 0
                self.twist.linear.z = 0
                self.twist.angular.x = 0
                self.twist.angular.y = 0
                self.twist.angular.z = 0.25 * \
                    self.normalize_angle(angle_to_goal - self.yaw)
                self.pub_vel.publish(self.twist)
                self.rate.sleep()
            else:
                print("distnce:", distance)
                self.twist.linear.x = + 0.07
                self.twist.linear.y = self.normalize_angle(
                    angle_to_goal - self.yaw) * 0.3
                self.twist.linear.z = 0
                self.twist.angular.x = 0
                self.twist.angular.y = 0
                self.twist.angular.z = 0.7 * \
                    self.normalize_angle(angle_to_goal - self.yaw)
                self.pub_vel.publish(self.twist)
                self.rate.sleep()

        print("i am at x,y ", self.x, self.y)
        self.stop(1)
        if self.status == 2:
            self.go_to_cage(goal_x, goal_y)

    def move_forward(self, speed, goal_distance):
        start_x = self.x
        start_y = self.y
        distance = 0

        print("inside move_forward, distance to travel:",
              goal_distance,  "start_x:", start_x, "start_y:", start_y)

        while distance < goal_distance and not rospy.is_shutdown():
            self.twist.linear.x = speed
            self.twist.linear.y = 0
            self.twist.linear.z = 0
            self.twist.angular.x = 0
            self.twist.angular.y = 0
            self.twist.angular.z = 0
            self.pub_vel.publish(self.twist)
            self.rate.sleep()
            distance = sqrt(pow((self.x - start_x), 2) +
                            pow((self.y - start_y), 2))

        self.twist.linear.x = 0
        self.pub_vel.publish(self.twist)

    def turn(self, target_angle):

        minus = 1
        if target_angle < 0:
            minus = -1
        #print("pocetni kut", self.yaw)
        last_angle = self.yaw
        turn_angle = 0
        command = Twist()
        target_rad = target_angle
        print("target angle:", target_rad)
        command.angular.z = self.kP * (target_rad - self.yaw)
        while abs(turn_angle) < abs(target_rad) and not rospy.is_shutdown() and target_angle > 0.01:
            command.angular.z = minus * 0.007 + minus * self.kP * \
                (abs(target_rad) - abs(turn_angle))
            self.pub_vel.publish(command)
            #print("turn speed:",  command.angular.z)
            #print("turn angle:", turn_angle, "target angle:", target_rad)
            delta_angle = self.normalize_angle(self.yaw - last_angle)
            turn_angle += delta_angle
            last_angle = self.yaw
            self.rate.sleep()

        command.angular.z = 0
        self.pub_vel.publish(command)

    def normalize_angle(self, angle):
        a = self.normalize_angle_positive(angle)
        if a > math.pi:
            a -= 2. * math.pi
        return a

    def normalize_angle_positive(self, angle):
        pi_2 = 2. * math.pi
        return fmod(fmod(angle, pi_2) + pi_2, pi_2)

    def takeoff(self):
        time.sleep(0.5)
        self.twist.angular.y = -90.0
        self.pub_camera_control.publish(self.twist)
        self.twist.angular.y = 0
        self.stop(2)
        takeoff = Empty()
        self.pub_takeoff.publish(takeoff)

    def land(self):
        self.stop(1)
        land = Empty()
        print("landing at", self.x)
        time.sleep(1)
        self.pub_land.publish(land)
        time.sleep(2)
        print("landed at", self.x)

    def exitAndLand(self):
        self.stop(0)
        print("Shutting down")
        self.pub_land.publish(Empty())


def main(fly):
    fly.takeoff()
    fly.fly()


if __name__ == "__main__":
    rospy.init_node('fly', anonymous=True)
    fly = Fly()
    main(fly)
    rospy.spin()
