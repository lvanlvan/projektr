#!/usr/bin/env python3
import sys
import cv2 as cv
import filter

#Potrebna su dva argumenta za rad programa, slika ili video nad kojim se vrsi detekcija
#te uvjet 0 ili 1 ovisno o tome koristimo li funkciju provjere pozicije kaveza
def main(argv):
    if len(argv) < 1:
        return -1

    cap = cv.VideoCapture(argv[0])
    check_position = int(argv[1])
    count = 0
    key = cv.waitKey(1)
    position = 0
    while key != 27:
        # src = cv.imread("ok.png")
        hasFrame, src = cap.read()
        if not hasFrame:
            cv.waitKey()
            break

        count += 1
        frame = cv.resize(src, (1422, 800))
        #Ova petlja je dodana zbog performansi rada programa, odnosno
        #nije potrebno provoditi filtraciju nad svakim frame-om
        #U unutarnjoj petlji se provodi filtracija dok se u vanjskoj petlji prikazuju oznake kaveza i bova
        if count % 1 == 0 or count == 1:   # prikaz 1. i svakog parnog vfrejma 
            #if count % 5 == 0 or count == 1:  
            dic_kavez = filter.kavez(frame, check_position)
            dic_bova = filter.bova(frame)

            centar_kavez = filter.detection_kavez(dic_kavez, frame)
            center_bova = filter.detection_bova(dic_bova, frame)
            key = cv.waitKey(1)
            #Pritiskom na tipku SPACEBAR mozemo ukloniti odnosno prikazati podatke o polozaju kaveza
            if key == 32:
                position = (position+1)%2
            if position==0:
                pass   #namjesitit ovo
                #filter.position(centar_kavez, center_bova,frame)
                #filter.find_anomaly(centar_kavez, center_bova, frame)
           
            #TREBA DODATNI BOLJE FILTRIRANJE, DA KAD JE NA SLICI SJAJ NA MORU DA GA NE DETEKTIRA KAO KAVEZ
            #DODATI DA ISPISUJE POMAK U METRIMA (VIDJETI KOLIKO SVAKI METAR IMA PIXELA), 
            #MOZE SE RECUNATI PREMA DIJAGONALI IZMEĐU BOVA ILI DIJAGONALI KAVEZA S TIM DA TREBA PRETVORITI TO IZ KOORDIATNOG
            #SUSTAVA U OBICNI TAKO DA AKO SE KAVEZ VIDI IZ NEKOG KUTA DA SE SVEJEDNO LIJEPO PRERACUNA
            
            if count == 1 or count % 10 == 0:   #svaki 10. ili 1 frejm gledamo 
                filter.find_anomaly(centar_kavez, center_bova, frame)
           
           
            cv.imshow("frame", frame)
            if cv.waitKey(100) & 0xFF == ord('q'):
                    break

    return 0

if __name__ == "__main__":
    main(sys.argv[1:])
