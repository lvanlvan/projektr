#!/usr/bin/env python
from __future__ import print_function
import math
from pickle import FALSE, TRUE
from re import A
import matplotlib.pyplot as plt

import numpy as np
from math import *


def lawnMower(start_x, start_y, start_angle, distance_x, distance_y, offset = 1):

    print("starting point:", start_x, start_y)
    data_x = [start_x]
    data_y = [start_y]

    n = int(distance_x / offset) * 2 + 1

    for i in range(n):

        if i % 4 == 0:
            angle = start_angle
        elif i % 4 == 1 or i % 4 == 3:
            angle = start_angle - math.pi/2
        else:
            angle = start_angle - math.pi

        if i % 2 == 0:
            data_x.append(data_x[-1] + distance_y * cos(angle))
            data_y.append(data_y[-1] + distance_y * sin(angle))
        else:
            data_x.append(data_x[-1] + offset * cos(angle))
            data_y.append(data_y[-1] + offset * sin(angle))

        print("going to point (x,y)", data_x[-1], data_y[-1])
    data_x.append(start_x)
    data_y.append(start_y)
    # plt.plot(data_x, data_y)
    # plt.axis()
    # plt.title("lawn mower algorithm")
    # plt.show()
    return [data_x, data_y]