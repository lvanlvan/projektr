#!/usr/bin/env python3
import sys
import cv2 as cv
import filter
import numpy as np
import math
#Potrebna su dva argumenta za rad programa, slika ili video nad kojim se vrsi detekcija
#te uvjet 0 ili 1 ovisno o tome koristimo li funkciju provjere pozicije kaveza
def main(argv):
    if len(argv) < 1:
        return -1

    cap = cv.VideoCapture(argv[0])
    check_position = int(argv[1])
    count = 0
    key = cv.waitKey(1)
    width  = cap.get(3)  # float `width`
    height = cap.get(4) 
    
    while key != 27:
       
        hasFrame, src = cap.read()
        if not hasFrame:
            cv.waitKey()
            break

        count += 1
        frame = src

        
        
        nasli = False    #AKO JE NASLI TRUE NASLI SMO KAVEZ I 4 BOVE
        bove_od_kaveza = False
       
        rijecnik_bove = nadjiNarancasto(frame)   #trazi bove
        rijecnik_kavez = nadiKavez(frame)        #trazi kavez
        tocka = provjeri(rijecnik_kavez, rijecnik_bove)
     
        tocka_prov = (-1, -1)
        if tocka_prov != tocka:
            #print(tocka)
            nasli = True
            bove_od_kaveza = provjeri_jesu_li_unutra(rijecnik_bove, tocka)   #provjerava jeli sreditse kaveza tj tocka unutar bovi
        if check_position == 1 and nasli == True:
            find_pomak(rijecnik_bove, rijecnik_kavez, nasli, frame)
            print("da")
        
        cv.imshow("frame", frame)
        if cv.waitKey(100) & 0xFF == ord('q'):
            break

    return 0
    
def check_if_inside(T, lik):
    n = len(lik)
    unutra = False
    x = T[0]
    y = T[1]
    prvatocka = lik[0]
    p1x = prvatocka[0]
    p1y = prvatocka[1]
    for i in range(n+1):
        tocka = lik[i % n]
        p2x = tocka[0]
        p2y = tocka[1]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xints:
                        unutra = not unutra
        p1x,p1y = p2x,p2y

    return unutra

def intersection(a1, a2, b1, b2):    
    s = np.vstack([a1,a2,b1,b2])        
    h = np.hstack((s, np.ones((4, 1)))) 
    l1 = np.cross(h[0], h[1])           
    l2 = np.cross(h[2], h[3])           
    x, y, z = np.cross(l1, l2)         
    if z == 0:  
                              # lines are parallel
        return (float('inf'), float('inf'))
    return (x/z, y/z)



def nadjiNarancasto(img):  #nadje narancaste stvari na slici
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    #mask = cv.inRange(hsv,(10, 100, 20), (25, 255, 255) )   #RADI ZA VECI RANGE NARANCASTE
    mask = cv.inRange(hsv,(10, 100, 200), (25, 255, 255) )
    
    kernel = np.ones((3, 3), np.uint8)
    mask = cv.erode(mask, kernel, iterations=1)
    mask = cv.dilate(mask, kernel, iterations=5)
    
    contours, _ = cv.findContours(mask.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    rijecnik_bove = {}
    id_b = 0
    for contour in contours: 
        approx = cv.approxPolyDP(contour, 0.01 * cv.arcLength(contour, True), True )  
        if len(approx) >= 8:
            cv.drawContours(img, [approx], 0, (10, 0, 200), 5)
            x = approx.ravel()[0]
            y = approx.ravel()[1] - 5
            cv.putText(img, "bova", (x, y), cv.FONT_HERSHEY_COMPLEX, 0.9, (0, 0, 0))
            x, y, w, h = cv.boundingRect(approx)
            srediste = (int((x + x + w)/ 2), int((y + y + h)/ 2))
            rijecnik_bove[id_b] = [srediste[0], srediste[1], w, h, x, y]
            id_b = id_b + 1
    return rijecnik_bove

   

    
    
def nadiKavez(img):
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    lower_white = np.array([0,0,200])
    #lower_white = np.array([0,0,180])  #OVO radi za veci range
    higher_white = np.array([255,50,255])
    mask = cv.inRange(hsv, lower_white, higher_white)
    kernel = np.ones((3, 3), np.uint8)
    mask = cv.erode(mask, kernel, iterations=1)
    mask = cv.dilate(mask, kernel, iterations=5)
    contours, _ = cv.findContours(mask.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    dic_kavez={}
    id_k=0
    
    for contour in contours: 
        approx = cv.approxPolyDP(contour, 0.01 * cv.arcLength(contour, True), True )  #arc l contour length and closed shake
        x = approx.ravel()[0]
        y = approx.ravel()[1] 
        if len(approx) >= 5:
            #cv.drawContours(img, [approx], 0, (100, 0, 200), 5)
            cv.putText(img, "kavez", (x, y), cv.FONT_HERSHEY_COMPLEX, 0.9, (0, 0, 0))
            tocka = (x, y)   #KOJA TREBA 
            
            #cv.circle(img, (x, y), 10, (0, 255 ,255), 3)
            x, y, w, h = cv.boundingRect(approx)
            srediste = (int((x + x + w)/ 2), int((y + y + h)/ 2))    #kad se vidi cijeli kavez je dobto, treba jos namjesitit
            cv.circle(img, srediste , 10, (0, 100 ,255), 3)
            cv.rectangle(img, (x, y), (x + w, y + h), (255, 255, 255), 4)
            dic_kavez[id_k] = [srediste[0], srediste[1], w, h, x, y]
    return dic_kavez

    

def provjeri(dic_kavez, dic_bove):
    tocka = (-1, -1)
    
    if len(dic_bove) >= 4:
        if len(dic_kavez) > 0:
            #print("DA")
            tocka = (dic_kavez[0][0], dic_kavez[0][1])

    return tocka
            
            
def find_pomak(dic_bova, dic_kavez, nasli, frame):
    if nasli == True:    #ima 4 bove i postoji kavez, 1 je kavez pa onda ne provjeravamojesu li bove od tog kaveza
         prva_bova  =  dic_bova[0]
         druga_bova =  dic_bova[1]
         treca_bova =  dic_bova[2]
         cetvrta_bova = dic_bova[3]
         sjecista=[]
         nasli_sjeciste = False
         tocka_A = (prva_bova[0], prva_bova[1])
         tocka_B = (druga_bova[0], druga_bova[1])
         tocka_C = (treca_bova[0], treca_bova[1])
         tocka_D = (cetvrta_bova[0], cetvrta_bova[1]) 
         
         #AB i CD
         s1 = intersection(tocka_A, tocka_B, tocka_C, tocka_D)
         sjecista.append(s1)
         #AC i BD
         s2 = intersection(tocka_A, tocka_C, tocka_B, tocka_D)
         sjecista.append(s2)
         #AD i BC
         s3 = intersection(tocka_A, tocka_D, tocka_B, tocka_C)
         sjecista.append(s3)  
         
         w = 800
         h = 1400 
         print(sjecista)
         for s in sjecista:      
             if s[0] < 0 or s[1] < 0:
                 continue
             else:
                 sjeciste = s
                 nasli_sjeciste = True
         
         #print(str(sjecista))
         #print("sjeciste: " +str(sjeciste))
         cv.circle(frame, (int(sjeciste[0]), int(sjeciste[1])), 10, (0, 255 ,255), 3)
         tolerancija = 15
         
         if nasli_sjeciste:
             pomak = math.sqrt(pow(sjeciste[0] - dic_kavez[0][0], 2)) + math.sqrt(pow(sjeciste[1] - dic_kavez[0][1], 2))
             if pomak < tolerancija:   #toleriramo pogresku 
                 print("nema pomaka")
             else:
                #cv.line(frame, (int(sjeciste[0]), int(sjeciste[1])), (dic_kavez[kavez][0], dic_kavez[kavez][1]), (0, 255, 0), 7)
                print("pomak je " + str(pomak))
         
         count_one = 0
         for bova in dic_bova:
             if count_one == 3:
                 break
             count_one = count_one + 1
             #spojiti svaku sa svakom 
             cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]), (prva_bova[0], prva_bova[1]), (255, 0, 255), 3)
             cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]), (druga_bova[0], druga_bova[1]), (255, 0, 255), 3)
             cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]), (treca_bova[0], treca_bova[1]), (255, 0, 255), 3)
             cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]), (cetvrta_bova[0], cetvrta_bova[1]), (255, 0, 255), 3)
             
             
def poslozi(A, B, C, D):
    lijeva = A
    arr=[A, C, B, D]
    for tocka in arr:
       if tocka[0] < lijeva[0]:
            lijeva = tocka
    desna = A
    for tocka in arr:
       if tocka[0] > desna[0]:
            desna  = tocka
    gornja = A
    for tocka in arr:
       if tocka[1] > gornja[1]:
            gornja = tocka
    donja = A
    for tocka in arr:
       if tocka[1]  < donja[1]:
            donja = tocka
    arr = [lijeva, donja, desna, gornja]
    return arr

def provjeri_jesu_li_unutra(dic_bova, srediste_kaveza):
    prva_bova  =  dic_bova[0]
    druga_bova =  dic_bova[1]
    treca_bova =  dic_bova[2]
    cetvrta_bova = dic_bova[3]
    
    tocka_A = (prva_bova[0], prva_bova[1])
    tocka_B = (druga_bova[0], druga_bova[1])
    tocka_C = (treca_bova[0], treca_bova[1])
    tocka_D = (cetvrta_bova[0], cetvrta_bova[1]) 
    arrP = poslozi(tocka_A, tocka_B, tocka_C, tocka_D)
    nasli_bove = check_if_inside(srediste_kaveza, arrP)
    return nasli_bove




if __name__ == "__main__":
    main(sys.argv[1:])

            
