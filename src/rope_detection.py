#!/usr/bin/env python3
import sys
import cv2 as cv
import numpy as np

def main(argv):
    if len(argv) < 1:
        return -1
    
    cap = cv.VideoCapture(argv[0])
    
    key = cv.waitKey(1)
    position = 0
    while key != 27:
        # src = cv.imread("ok.png")
        hasFrame, src = cap.read()
        if not hasFrame:
            cv.waitKey()
            break
        
        # Promjena velicine slike.
        # 1422x800 su dimenzije prilagodene za Bebop
        # 800x600 su dimenzije prilagodene za dataset
        # frame = cv.resize(src, (1422, 800))
        frame = cv.resize(src, (800, 600))
        
        # Odredi crna (tamna) podrucja
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        ret, thresh = cv.threshold(gray, 60, 255, cv.THRESH_BINARY_INV)
        
        # Na osnovu detektiranih crnih (tamnih) podrucja detektiraj linije
        # Linije ce biti oznacene crvenom bojom
        lines = cv.HoughLinesP(thresh, 1, np.pi/180,100, minLineLength=20, maxLineGap=2)
        if lines is not None:
            for line in lines:
                x1, y1, x2, y2 = line[0]
                cv.line(frame, (x1, y1), (x2, y2), (0, 0, 255), 3)
        
        key = cv.waitKey(1)
        
        # cv.imshow("thresh", thresh)
        cv.imshow("Detected ropes", frame)

if __name__ == "__main__":
    main(sys.argv[1:])
