#!/usr/bin/env python3
import cv2 as cv
import numpy as np
import math
from matplotlib import path
import matplotlib.pyplot as plt

#Funkcija kavez primjenjuje filtere za dobivanje korisnih podataka u slucaju detekcije kaveza
#Kao argumente prima sliku i uvjet provodi li se provjera pozicije kaveza
#Uvjet je dodan zbog funckionalnosti programa jer su se u vrijeme izrade
#programa koristile modificirane slike te nesavrsenog rada samog programa
#Vraca dictionary koji sadrzi podatke o pravokutnicima potrebene za njihovo iscrtavanje
def kavez(frame, check_position):
    scale = 1
    delta = 0
    ddepth = cv.CV_16S

    #Pretvorba u slike u HSV model za lakšu manipulaciju
    #Zadaju se granice za svaki parametar kako bi izdvojili korisne informacije
    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    l_h = 0
    l_s = 0
    l_v = 0

    u_h = 179
    u_s = 50
    if check_position == 1:
        u_v = 200
    else:
      u_v = 251

    #Primjena maske na originalnu sliku
    l_b = np.array([l_h, l_s, l_v])
    u_b = np.array([u_h, u_s, u_v])
    mask = cv.inRange(hsv, l_b, u_b)
    hsv = cv.bitwise_and(frame, frame, mask=mask)

    #Pretvorba u sivu sliku za potrebe traženja kontura
    gray = cv.cvtColor(hsv, cv.COLOR_BGR2GRAY)

    #Primjena niskopropusnih filtera kako bi se uklonio šum
    #Po potrebi isprobati druge filtere i/ili parametre
    blur = cv.GaussianBlur(gray, (15, 15), 0)

    #Funkcija Sobel koristi se za prepoznavanje rubova na slici
    #Po potrebi isprobati druge metode i/ili parametre
    grad_x = cv.Sobel(blur, ddepth, 1, 0, ksize=3, scale=scale, delta=delta, borderType=cv.BORDER_DEFAULT)
    grad_y = cv.Sobel(blur, ddepth, 0, 1, ksize=3, scale=scale, delta=delta, borderType=cv.BORDER_DEFAULT)
    abs_grad_x = cv.convertScaleAbs(grad_x)
    abs_grad_y = cv.convertScaleAbs(grad_y)
    grad = cv.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)

    #Primjena morfoloskih operacija za dodatno filtriranje suma
    kernel = np.ones((3, 3), np.uint8)
    erosion = cv.erode(grad, kernel, iterations=1)

    _, threshold = cv.threshold(erosion, 2, 255, cv.THRESH_BINARY)

    #Prepoznavanje kontura na slici te njihova ispuna kako bi dobili objekte
    contours, _ = cv.findContours(threshold, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    black = np.zeros((800, 1422, 1), dtype="uint8")
    image = []
    for contour in contours:
        approx = cv.approxPolyDP(contour, 0, True)
        if len(contour) > 0 and cv.contourArea(approx) > 5000:
            image = cv.drawContours(black, [approx], -1, (255, 255, 255), thickness=cv.FILLED)

    #Ponovno se primjenjuje prepoznavanje kontura, ovoga puta za odvajanje objekta dobivenih u proslom slucaju
    #Na dobivenim objektima se iscrtavaju elipse koje se ovisno o
    #njenim parametrima koriste za filtriranje lazne detekcije
    #Oko svake elipse se zatim crta pravokutnik ciji se paramteri spremaju u dictionary

    #black = cv.morphologyEx(black, cv.MORPH_CLOSE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (15, 15)))
    contours, _ = cv.findContours(black, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    dic = {}
    t = 0
    for contour in contours:
        blank = np.zeros((800, 1422, 1), dtype="uint8")
        if cv.contourArea(contour) > 10000:
            ellipse = cv.fitEllipse(contour)
            if ellipse[1][1] / ellipse[1][0] > 0.2 and ellipse[1][1] / ellipse[1][0] < 5:
                t += 1
                object = cv.ellipse(blank, ellipse, (255, 255, 255), -1)
                a, b, c, d = cv.boundingRect(object)
                dic.update({t: [a, b, c, d]})
                #print("a: " + str(a) + " b: " + str(b) + " c: " + str(c) +  " d: " + str(d))
                image = cv.drawContours(image, contours, -1, (255, 0, 0), thickness=1)
                #cv.imshow("konture", blank)

    #Funkcija vraca dictionary koji se koristi kod poziva
    return dic


def bova(frame):
    scale = 1
    delta = 0
    ddepth = cv.CV_16S

    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    l_h = 0
    l_s = 80
    l_v = 206

    u_h = 20
    u_s = 255
    u_v = 255

    l_b = np.array([l_h, l_s, l_v])
    u_b = np.array([u_h, u_s, u_v])
    mask1 = cv.inRange(hsv, l_b, u_b)
    l_b = np.array([170, 80, 50])
    u_b = np.array([u_h, u_s, u_v])
    mask2 = cv.inRange(hsv, l_b, u_b)
    mask = mask1 + mask2
    hsv = cv.bitwise_and(frame, frame, mask=mask)

    gray = cv.cvtColor(hsv, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (15, 15), 0)

    grad_x = cv.Sobel(blur, ddepth, 1, 0, ksize=3, scale=scale, delta=delta, borderType=cv.BORDER_DEFAULT)
    grad_y = cv.Sobel(blur, ddepth, 0, 1, ksize=3, scale=scale, delta=delta, borderType=cv.BORDER_DEFAULT)

    abs_grad_x = cv.convertScaleAbs(grad_x)
    abs_grad_y = cv.convertScaleAbs(grad_y)

    grad = cv.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)


    contours, _ = cv.findContours(grad, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    black = np.zeros((800, 1422, 1), dtype="uint8")
    for contour in contours:
        approx = cv.approxPolyDP(contour, 0, True)
        #if len(contour) > 100 and cv.contourArea(approx) > 1000:
        image = cv.drawContours(black, [approx], -1, (255, 255, 255), thickness=cv.FILLED)

    contours, _ = cv.findContours(black, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    dic = {}
    t = 0
    for contour in contours:
        skip = 0
        if cv.contourArea(contour) > 150:
            #print(cv.contourArea(contour))
            ellipse = cv.fitEllipse(contour)
            #if ellipse[1][1] / ellipse[1][0] > 0.2 and ellipse[1][1] / ellipse[1][0] < 5:
            t += 1
            cv.ellipse(image, ellipse, (0, 0, 255), -1)
            a, b, c, d = cv.boundingRect(contour)
            dic.update({t: [a, b, c, d]})
            image = cv.drawContours(image, contours, -1, (255, 0, 0), thickness=1)
    #cv.imshow("black", black)

    return dic


#Funkcija detection_kavez iscrtava pravokutnike te racuna centar
#Kao argumente prima dictionary s podacima o pravokutnicima te sliku
def detection_kavez(dic_kavez, frame):
    try:
        center = {}
        for y in dic_kavez:
            #Ovaj dio petlje se koristi kako bi uklonili ugnijezdene pravokutnike do kojih dolazi zbog nesavrsenstva
            skip = 0
            for x in dic_kavez:
                if dic_kavez[y][0] >= dic_kavez[x][0] and dic_kavez[y][1] >= dic_kavez[x][1] and dic_kavez[y][0] + \
                        dic_kavez[y][2] <= dic_kavez[x][0] + \
                        dic_kavez[x][2] and \
                        dic_kavez[y][1] + dic_kavez[y][3] <= dic_kavez[x][1] + dic_kavez[x][3] and y != x:
                    skip = 1
            if not skip:
                rect = cv.rectangle(frame, (dic_kavez[y][0], dic_kavez[y][1]),
                                    (dic_kavez[y][0] + dic_kavez[y][2], dic_kavez[y][1] + dic_kavez[y][3]),
                                    (0, 0, 82), 2)
                text = cv.putText(frame, "Kavez",
                                  (int(dic_kavez[y][0] + dic_kavez[y][2] / 2), int(dic_kavez[y][1] + dic_kavez[y][3] / 2)),
                                  cv.FONT_HERSHEY_COMPLEX, 1, (0, 0, 82))
                center.update(
                    {y: [int(dic_kavez[y][0] + dic_kavez[y][2] / 2), int(dic_kavez[y][1] + dic_kavez[y][3] / 2),
                         int(dic_kavez[y][2]/2), int(dic_kavez[y][3]/2), int(dic_kavez[y][0]), int(dic_kavez[y][1])]})
                #print("c:"+str(dic_kavez[y][2]) + "d:"+str(dic_kavez[y][3]))
    except:
        pass

    return center


def detection_bova(dic_kavez, frame):
    try:
        center = {}
        for y in dic_kavez:
            skip = 0
            for x in dic_kavez:
                if dic_kavez[y][0] >= dic_kavez[x][0] and dic_kavez[y][1] >= dic_kavez[x][1] and dic_kavez[y][0] + \
                        dic_kavez[y][2] <= dic_kavez[x][0] + \
                        dic_kavez[x][2] and \
                        dic_kavez[y][1] + dic_kavez[y][3] <= dic_kavez[x][1] + dic_kavez[x][3] and y != x:
                    skip = 1
            if not skip:
                rect = cv.rectangle(frame, (dic_kavez[y][0], dic_kavez[y][1]),
                                    (dic_kavez[y][0] + dic_kavez[y][2], dic_kavez[y][1] + dic_kavez[y][3]),
                                    (0, 255, 0), 2)
                text = cv.putText(frame, "Bova",
                                  (int(dic_kavez[y][0] + dic_kavez[y][2] / 2), int(dic_kavez[y][1] + dic_kavez[y][3] / 2)),
                                  cv.FONT_HERSHEY_COMPLEX, 1, (10, 30, 0))
                center.update({y: [int(dic_kavez[y][0] + dic_kavez[y][2] / 2), int(dic_kavez[y][1] + dic_kavez[y][3] / 2)]})
    except:
        pass

    return center


#Funkcija position koristi se pri odredivanju pravilnog polozaja kaveza
#Kao argumente prima dva dictionary-a s podacima o pravokutnicima te sliku
#Potrebna je dorada kako bi funkcija radila kada je prikazano vise kaveza
#Zbog manjka materijala funckija je napravljena na modificiranoj slici s jednim kavezom
def position(dic_kavez, dic_bova, frame):
    try:
        dic_bova = {k: v for k, v in sorted(dic_bova.items(), key=lambda item: item[1])}
        list = []
        for x in dic_bova:
            list.append(dic_bova[x])
        x1 = min(list[0][0], list[1][0], list[2][0], list[3][0])
        x2 = max(list[0][0], list[1][0], list[2][0], list[3][0])
        y1 = min(list[0][1], list[1][1], list[2][1], list[3][1])
        y2 = max(list[0][1], list[1][1], list[2][1], list[3][1])

        black = np.zeros((800, 1422, 1), dtype="uint8")
        cv.line(black, (list[0][0], list[0][1]), (list[1][0], list[1][1]), (255, 255, 255), 2)
        cv.line(black, (list[0][0], list[0][1]), (list[2][0], list[2][1]), (255, 255, 255), 2)
        cv.line(black, (list[1][0], list[1][1]), (list[3][0], list[3][1]), (255, 255, 255), 2)
        cv.line(black, (list[2][0], list[2][1]), (list[3][0], list[3][1]), (255, 255, 255), 2)
        cv.line(frame, (list[0][0], list[0][1]), (list[1][0], list[1][1]), (255, 255, 255), 2)
        cv.line(frame, (list[0][0], list[0][1]), (list[2][0], list[2][1]), (255, 255, 255), 2)
        cv.line(frame, (list[1][0], list[1][1]), (list[3][0], list[3][1]), (255, 255, 255), 2)
        cv.line(frame, (list[2][0], list[2][1]), (list[3][0], list[3][1]), (255, 255, 255), 2)

        #rect = cv.rectangle(black, (x1, y1), (x2, y2), (255, 255, 255), 2)
        contours, _ = cv.findContours(black, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        for contour in contours:
            approx = cv.approxPolyDP(contour, 0, True)
            cv.drawContours(black, [approx], -1, (255, 255, 255), thickness=cv.FILLED)

        contours, _ = cv.findContours(black, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        for contour in contours:
            ellipse = cv.fitEllipse(contour)
            #cv.ellipse(frame, ellipse, (0, 0, 255), 1)
        #cv.imshow("black",black)

        x0 = int((x2+x1)/2)
        y0 = int((y2+y1)/2)
        #print(x0, y0)

        for n in dic_kavez:
            x0_kavez = dic_kavez[n][0]
            y0_kavez = dic_kavez[n][1]
            a = dic_kavez[n][2]
            b = dic_kavez[n][3]
            radius = (a+b)/2
            x = dic_kavez[n][4]
            # print(x)
            # print(a)
            # print(x0_kavez)
            limit = x+2*a
            while x < limit:
                y1 = int(math.sqrt(pow(a,2)*(1-pow((x-x0_kavez)/b,2)))+y0_kavez)
                y2 = int(-math.sqrt(pow(a,2)*(1-pow((x-x0_kavez)/b,2)))+y0_kavez)
                x += 1
                if cv.pointPolygonTest(contour, (x, y1), False) == -1:
                    #print("Vani: " + str([x, y1]))
                    image = cv.circle(frame, (x, y1), radius=3, color=(0, 0, 255), thickness=-1)
                if cv.pointPolygonTest(contour, (x, y2), False) == -1:
                    #print("Vani: " + str([x, y2]))
                    image = cv.circle(frame, (x, y2), radius=3, color=(0, 0, 255), thickness=-1)

        print(x0_kavez, y0_kavez)

        pm_ratio = 20/radius
        pomak = round(math.sqrt(pow(x0_kavez-x0, 2) + pow(y0_kavez-y0, 2))*pm_ratio, 2)
        if pomak > 0.2:
            cv.putText(frame, "Kavez pomaknut " + str(pomak) + "m", (0, 40), cv.FONT_HERSHEY_COMPLEX, 1, (0, 0, 82))
        print(pomak)
    except:
        pass
        
"""~~~Trazi presjek 2 pravca kad imamo 2 tocke za svaki pravac
#     t1 i t2 tocke prvog pravca , t3 i t4 tocke drugog pravca"""
def intersection(a1, a2, b1, b2):    
    s = np.vstack([a1,a2,b1,b2])        
    h = np.hstack((s, np.ones((4, 1)))) 
    l1 = np.cross(h[0], h[1])           
    l2 = np.cross(h[2], h[3])           
    x, y, z = np.cross(l1, l2)         
    if z == 0:  
                              # lines are parallel
        return (float('inf'), float('inf'))
    return (x/z, y/z)


"""~~~Trazi neku fiksnu točku na kavezu, u ovom slučaju točku koja je najbliže tocki koja je na lijevom rubu slike, u sredini, dakle (0, height/2) 
       računa se tražeći najlijeviju bovu koja je orva tocka pravca, a centar kaveza druga tocka tog istog pravca
       potom se uzme lijevi vrh kvadrata koji se crta oko kaveza kao 1. točka 2. pravca, a 2. točka ima isti x kao i vrh, a y mu je
       0, da ne tražimo donji dio kvadrata jer nam samo i onako treba taj pravac"""
def get_point(centar, A, B, C, D, vrh, y):    
    lijeva = A                        
    arr=[A, C, B, D]                 
    for tocka in arr:
       if tocka[0] < lijeva[0]:
            lijeva = tocka       
    vrh_dva = (vrh[0], 0)
    lijeva = (0 , y/2)
    tocka = intersection(centar, lijeva, vrh, vrh_dva)
    tocka = (int(tocka[0]), int(tocka[1]))
    
    return tocka
    
"""~~~Funkcija poredava točke tako da kad ih se čita redom dobijemo četverokut a ne neki drugi lik"""
def poslozi(A, B, C, D):
    lijeva = A
    arr=[A, C, B, D]
    for tocka in arr:
       if tocka[0] < lijeva[0]:
            lijeva = tocka
    desna = A
    for tocka in arr:
       if tocka[0] > desna[0]:
            desna  = tocka
    gornja = A
    for tocka in arr:
       if tocka[1] > gornja[1]:
            gornja = tocka
    donja = A
    for tocka in arr:
       if tocka[1]  < donja[1]:
            donja = tocka
    arr = [lijeva, donja, desna, gornja]
    return arr

""""~~~Funkcija provjerava jeli neka tocka unutar nekog mnogokuta
    U ovom slučaju provjerava jeli središte kaveza unutar četverokuta kojeg čine bove
    U slučaju da nije, znamo da to nisu onda bove od pripadajućeg kaveza
    T- tokca za koju gledamo jeli unutar mnogokuta"""
def check_if_inside(T, lik):
    n = len(lik)
    unutra = False
    x = T[0]
    y = T[1]
    prvatocka = lik[0]
    p1x = prvatocka[0]
    p1y = prvatocka[1]
    for i in range(n+1):
        tocka = lik[i % n]
        p2x = tocka[0]
        p2y = tocka[1]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xints:
                        unutra = not unutra
        p1x,p1y = p2x,p2y

    return unutra

        
def find_anomaly(dic_kavez, dic_bova, frame):   #u slucaju vise  kaveza
    try:
        lista = {}  #lista u kojoj je kljuc kavez, a value je lista koja sadrzi bove i 
                     #njezine udaljenosti od kaveza sortirano uzlazno (dakle najblize 4 su bove od kaveza)
        for kavez in dic_kavez:
            
            if len(dic_kavez) == 1:
                bbbbb=7
                #position(dic_kavez, dic_bova, frame)
                #return;
            udaljenosti=[]
            bove_dva={}
            centar_x = dic_kavez[kavez][0]
            centar_y = dic_kavez[kavez][1]
            for bova in dic_bova:
                centar_bova_x= dic_bova[bova][0]
                centar_bova_y= dic_bova[bova][1]
                udaljenost = math.sqrt(pow(centar_x - centar_bova_x, 2)) + math.sqrt(pow(centar_y - centar_bova_y, 2))
                udaljenosti.append(udaljenost)
                bove_dva[bova] = udaljenost
            bove_dva=dict(sorted(bove_dva.items(), key=lambda item: item[1]))
            lista[kavez]= bove_dva
            
        
        print(str(dic_kavez))
        for kavez in dic_kavez:
            bove_sortirano = lista[kavez]
            if len(bove_sortirano) >=4:  #ako je veće ili jednako 4 prvih 4 su bove od zadanog kaveza
                
                  #kljuc prve bove tj id prve bove
                prva_bova =list(bove_sortirano.keys())[0]
                druga_bova =list(bove_sortirano.keys())[1]
                treca_bova =list(bove_sortirano.keys())[2]
                cetvrta_bova =list(bove_sortirano.keys())[3]
                count_one = 0
                
                nasli_sjeciste = False
                tocka_A = (dic_bova[prva_bova][0], dic_bova[prva_bova][1])
                tocka_B = (dic_bova[druga_bova][0], dic_bova[druga_bova][1])  
                tocka_C = (dic_bova[treca_bova][0], dic_bova[treca_bova][1])
                tocka_D = (dic_bova[cetvrta_bova][0], dic_bova[cetvrta_bova][1])  
                
                #pogledaj jesu li te bove od tog kaveza , ako je nasli_boeve = true onda jesu
                print("------------------------")
                print("KAVEZ " + str(kavez))
                srediste_kaveza=(dic_kavez[kavez][0], dic_kavez[kavez][1])
                print("SREDISTE JE " + str(srediste_kaveza))
                
                arrP = poslozi(tocka_A, tocka_B, tocka_C, tocka_D)
                nasli_bove = check_if_inside(srediste_kaveza, arrP)
                print("Kavez ima sve 4 bove vidljive : " + str(nasli_bove))
                
                #ako smo nasli bove od kaveza onda ih spajamo međusobno 
                if nasli_bove== True:
                    for bova in bove_sortirano:
                        if count_one == 3:
                             break
                        count_one = count_one + 1
                        #spojiti svaku sa svakom 
                        cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]), (dic_bova[prva_bova][0], dic_bova[prva_bova][1]), (255, 0, 255), 3)
                        cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]), (dic_bova[druga_bova][0], dic_bova[druga_bova][1]), (255, 0, 255), 3)
                        cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]), (dic_bova[treca_bova][0], dic_bova[treca_bova][1]), (255, 0, 255), 3)
                        cv.line(frame, (dic_bova[bova][0], dic_bova[bova][1]), (dic_bova[cetvrta_bova][0], dic_bova[cetvrta_bova][1]), (255, 0, 255), 3)
                    
                    h = 800   #height
                    w = 1422  #width
                    #width = frame.get(3)
                    #height = frame.get(4)
                    
                    
                    #trazenje neke tocke na kavezu
                    vrh = (dic_kavez[kavez][4], dic_kavez[kavez][5]) 
                    tocka =get_point(srediste_kaveza, tocka_A, tocka_B, tocka_C, tocka_D, vrh, h)    #TAMO GDJE DRON TREBA ICI
                    print("TOCKA NA KAVEZU JE " + str(tocka))
                    cv.circle(frame, tocka, 10, (0, 100 ,200), 3)
                    
                    #trazenje sjecista
                    sjecista=[]
                 
                    #AB i CD
                    s1 = intersection(tocka_A, tocka_B, tocka_C, tocka_D)
                    sjecista.append(s1)
                    #AC i BD
                    s2 = intersection(tocka_A, tocka_C, tocka_B, tocka_D)
                    sjecista.append(s2)
                    #AD i BC
                    s3 = intersection(tocka_A, tocka_D, tocka_B, tocka_C)
                    sjecista.append(s3)
                    
                    
                    
                    for s in sjecista:
                        if abs(s[0]) > w or abs(s[1]) > h:
                            #print("NIJE SJECISTE: " + str(s[0]) + "  > " + str(h) + " " + str(s[1]) + " > " + str(w)) 
                            continue
                        sjeciste = s
                        nasli_sjeciste = True
                    print("SJECISTE "+ str(nasli_sjeciste))
                   
                    #gledamo jeli sjeciste cetverokuta kojeg cine bove priblizno na istom mjestu (uzimamo neku toleranciju )kao 
                    # i centar kaveza, ako je nema pomaka, ako nije vidljiv je pomak
                    tolerancija = 7
                    if nasli_sjeciste:
                        pomak = math.sqrt(pow(sjeciste[0] - dic_kavez[kavez][0], 2)) + math.sqrt(pow(sjeciste[1] - dic_kavez[kavez][1], 2))
                        if pomak < tolerancija:   #toleriramo pogresku 
                            print("nema pomaka")
                        else:
                            cv.line(frame, (int(sjeciste[0]), int(sjeciste[1])), (dic_kavez[kavez][0], dic_kavez[kavez][1]), (0, 255, 0), 7)
                            print("pomak je " + str(pomak))
                
                
                
                
                
                
                    
                    
                 
    except:
        pass
